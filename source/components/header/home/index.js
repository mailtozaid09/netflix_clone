import React from 'react';
import { View,  Image, TouchableOpacity, StyleSheet } from 'react-native';

import { Container, Icon, Text } from './styles';

import { media } from '../../../global/media';

import { useDispatch, useSelector } from 'react-redux';

import { signOut } from '../../../store/modules/auth/actions';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { InstagramSans } from '../../../global/fontFamily';
import { colors } from '../../../global/colors';

const Header = ({navigation}) => {
    const dispatch = useDispatch();

    const user = useSelector(state => state.auth.user);

    const current_profile = useSelector(state => state.profile.current_profile);


    return (
        <Container>
            <Text style={styles.title} >For {current_profile?.user_name}</Text>
            
            <View style={{flexDirection: 'row'}} >
                <TouchableOpacity
                    onPress={() => {navigation.navigate('Search')}}
                >
                    <Image source={media.search} style={{height: 24, width: 24}} />
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {navigation.navigate('ProfileSettings')}}
                    style={{marginLeft: 20}}
                >
                    <Image source={current_profile?.user_icon} style={{height: 28, width: 28}} />
                </TouchableOpacity>
            </View>


        </Container>
    );
}

const styles = StyleSheet.create({
    title: {
        fontSize: 24,
        color: colors.white,
        fontFamily: InstagramSans.Medium,
    }
})

export default Header