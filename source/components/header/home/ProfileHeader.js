import React from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet } from 'react-native'

import { media } from '../../../global/media'

import BackArrow from 'react-native-vector-icons/Feather';
import { colors } from '../../../global/colors';
import { useSelector } from 'react-redux';
import { InstagramSans } from '../../../global/fontFamily';


const ProfileHeader = ({navigation}) => {
    const current_profile = useSelector(state => state.profile.current_profile);


    return (
        <View style={{width: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 0,}} >
            <TouchableOpacity
                    onPress={() => {navigation.goBack()}}
                >
                    <BackArrow name="arrow-left" size={28}  color={colors.white} />
                </TouchableOpacity>
            <View>
            <Text style={styles.title} >{current_profile?.user_name}</Text>
            </View>
            <Text></Text>
        </View>
    )
}

const styles = StyleSheet.create({
    title: {
        fontSize: 24,
        color: colors.white,
        fontFamily: InstagramSans.Medium,
    }
})


export default ProfileHeader