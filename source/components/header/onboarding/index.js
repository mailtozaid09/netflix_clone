import React from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet } from 'react-native'

import { media } from '../../../global/media'



const Header = ({navigation}) => {
    return (
        <View style={{width: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 0,}} >
            <View>
                <Image source={media.netflixIcon} style={{height: 40, width: 40, resizeMode: 'contain'}}  />
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center', }} >
                <TouchableOpacity
                    onPress={() => {}}
                >
                    <Text style={{fontSize: 16, color: 'white', fontWeight: '600', }} >PRIVACY</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => {navigation.navigate('LoginStack')}}
                >
                    <Text style={{fontSize: 16, color: 'white', fontWeight: '600', marginLeft: 20 }} >SIGN IN</Text>
                </TouchableOpacity>
            </View>
            
        </View>
    )
}

export default Header