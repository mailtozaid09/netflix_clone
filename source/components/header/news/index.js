import React from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet } from 'react-native'

import { media } from '../../../global/media'
import { colors } from '../../../global/colors'
import { InstagramSans } from '../../../global/fontFamily'



const NewsAndHotHeader = ({navigation,}) => {
    return (
        <View style={{width: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 0,}} >
            <View>
                <Text style={styles.title} >News & Hot</Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center', }} >
                <TouchableOpacity
                    onPress={() => {}}
                >
                    <Text style={styles.helpText} >HELP</Text>
                </TouchableOpacity>
            </View>
            
        </View>
    )
}

const styles = StyleSheet.create({
    helpText: {
        fontSize: 14, 
        color: colors.white,
        fontFamily: InstagramSans.Medium,
    },
    title: {
        fontSize: 28, 
        color: colors.white,
        fontFamily: InstagramSans.Bold,
    }
})

export default NewsAndHotHeader