import React from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet } from 'react-native'

import { media } from '../../../global/media'

import BackArrow from 'react-native-vector-icons/Feather';
import { colors } from '../../../global/colors';
import { InstagramSans } from '../../../global/fontFamily';

const AddWatchingHeader = ({navigation, inputValue, onCancelFunction, onSaveFunction}) => {
    return (
        <View style={{width: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 10, marginBottom: 30}} >
            <TouchableOpacity
                    onPress={onCancelFunction}
                >
                <Text style={styles.edit} >Cancel</Text>
            </TouchableOpacity>

            <Text style={styles.title} >Add Profile</Text>
            
            <TouchableOpacity
                    onPress={onSaveFunction}
                    disabled={!inputValue ? true : false}
                >
                <Text style={[styles.edit, {color: inputValue ? colors.white : colors.grey }]} >Save</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    title: {
        fontSize: 22,
        color: colors.white,
        fontFamily: InstagramSans.Medium
    },
    edit: {
        fontSize: 18,
        color: colors.white,
        fontFamily: InstagramSans.Regular
    }
})

export default AddWatchingHeader