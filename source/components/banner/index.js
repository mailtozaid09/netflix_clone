import React from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet } from 'react-native'

import { media } from '../../global/media'
import { colors } from '../../global/colors'
import { InstagramSans } from '../../global/fontFamily'
import { screenHeight } from '../../global/constants'



const Banner = ({navigation, isAccountCreated}) => {
    return (
        <View style={{width: '100%', padding: 10, marginTop: 90 }} >
            <View style={{}} >
               
                <Image source={media.banner} style={{borderWidth: 1.5, borderRadius: 8, borderColor: colors.light_gray, height: screenHeight*0.6, width: '100%', }} />

                <View style={{position: 'absolute', alignItems: 'center', width: '100%', top: 40}} >
                    <View style={{flexDirection: 'row', alignItems: 'center'}} >
                        <Image source={media.netflixIcon} style={{height: 24, width: 24, resizeMode: 'contain'}} />
                        <Text style={styles.seriesText} >SERIES</Text>
                    </View>
                </View>

                <View style={{position: 'absolute', width: '100%', bottom: 30}} >
                    <View style={{flex: 1, margin: 20, marginBottom: 0, alignItems: 'center',}} >
                        

                        <Text style={styles.movieTitle} >YOU</Text>

                        <View style={{flexDirection: 'row', alignItems: 'center'}} >
                            <Text style={styles.movieGenre} >Mind-Bending </Text>
                            <Text style={styles.dotStyle} >{'\u2B24'}</Text> 
                            <Text style={styles.movieGenre} >Suspenseful</Text>
                            <Text style={styles.dotStyle} >{'\u2B24'}</Text> 
                            <Text style={styles.movieGenre} >Sci-fi</Text>
                        </View>
                       

                        <View style={{flexDirection: 'row', alignItems: 'center', width: '100%', justifyContent: 'space-between'}} >
                            <TouchableOpacity 
                                onPress={() => {}} 
                                style={[styles.buttonContainer, { marginRight: 10, backgroundColor: colors.white,}]} >
                                <Image source={media.play} style={{height: 30, width: 30}} />
                                <Text style={styles.buttonText} >Play</Text>
                            </TouchableOpacity>
                            <TouchableOpacity 
                                onPress={() => {}}
                                style={[styles.buttonContainer, { marginLeft: 10, backgroundColor: colors.accent,}]} >
                                <Image source={media.plus} style={{height: 24, width: 24}} />
                                <Text style={[styles.buttonText, {color: colors.white}]} >My List</Text>
                            </TouchableOpacity>
                        </View>
                        
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    helpText: {
        fontSize: 14, 
        color: colors.black,
        fontFamily: InstagramSans.Medium,
    },
    seriesText: {
        fontSize: 14, 
        color: colors.white,
        letterSpacing: 3,
        marginBottom: 6,
        fontFamily: InstagramSans.Bold,
    },
    movieTitle: {
        fontSize: 40, 
        color: colors.white,
        letterSpacing: 3,
        marginBottom: 6,
        fontFamily: InstagramSans.Bold,
    },
    dotStyle: {
        color: colors.white,
        fontSize: 6,
        marginHorizontal: 8,
        marginBottom: 5
    },
    movieGenre: {
        fontSize: 16, 
        color: colors.white,
        marginBottom: 6,
        fontFamily: InstagramSans.Bold,
    },
    buttonText: {
        fontSize: 18, 
        color: colors.black,
        marginLeft: 8,
        fontFamily: InstagramSans.Bold,
    },
    buttonContainer: {
        height: 44,
        flex: 1,
        marginTop: 10,
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    }
})

export default Banner