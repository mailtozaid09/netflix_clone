import React from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet, TextInput } from 'react-native'
import { colors } from '../../global/colors'
import { InstagramSans } from '../../global/fontFamily'



const Input = ({title, placeholder, value, onShowPass, showPass, onChangeText, isPassword, inputColor}) => {
    return (
        <View>
            <Text style={[styles.title, {color: inputColor ? colors.black : colors.white}]} >{title}</Text>
            <View style={[styles.container, inputColor ? {borderWidth: 1, } : {backgroundColor: colors.accent}]} >
                <TextInput
                    autoCapitalize="none"
                    autoCorrect={false}
                    style={[styles.inputStyle, {color: inputColor ? colors.black : colors.white} ]}
                    placeholder={placeholder}
                    onChangeText={onChangeText}
                    placeholderTextColor={colors.grey}
                    secureTextEntry={isPassword && !showPass ? true : false}
                />
                 {isPassword &&
                    <TouchableOpacity
                        onPress={onShowPass}
                    >
                        <Text style={{color: colors.grey, fontFamily: InstagramSans.Regular}} >{!showPass ? 'SHOW' : 'HIDE'}</Text>
                    </TouchableOpacity>
                }
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 4,
        paddingHorizontal: 20,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    inputStyle: {
        flex: 1,
        height: 50,
        marginRight: 20,
        fontSize: 16,
        fontFamily: InstagramSans.Bold
    },
    title: {
        fontSize: 16,
        color: colors.black,
        marginBottom: 8,
        fontFamily: InstagramSans.Bold
    }
})

export default Input