import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import ProfileScreen from '../../screens/profile';
import { fontSize } from '../../global/fontFamily';


const Stack = createStackNavigator();


const ProfileStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Profile" 
        >
            <Stack.Screen
                name="Profile"
                component={ProfileScreen}
                options={{
                    headerShown: true,
                    headerTitle: 'Profile',
                    // headerLeft: () => null,
                    // headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                }}
            />
            {/* <Stack.Screen
                name="EditProfile"
                component={EditProfile}
                options={{
                    headerShown: true,
                    headerTitle: 'Profile',
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                }}
            />
            
            <Stack.Screen
                name="Wishlist"
                component={WishlistScreen}
                options={({navigation, route}) => ({
                    headerShown: true,
                    headerTitle: 'Wishlist',
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                    headerLeft: () => (<BackButton navigation={navigation} />),
                })}
            />
            <Stack.Screen
                name="UserOrders"
                component={UserOrders}
                options={({navigation, route}) => ({
                    headerShown: true,
                    headerTitle: 'My Orders',
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                    headerLeft: () => (<BackButton navigation={navigation} />),
                })}
            />
            <Stack.Screen
                name="OrderDetails"
                component={OrderDetails}
                options={({navigation, route}) => ({
                    headerShown: true,
                    headerTitle: 'Order Details',
                    headerTitleStyle: {fontSize: fontSize.Title, fontWeight: '700'},
                    headerLeft: () => (<BackButton navigation={navigation} />),
                })}
            /> */}
        </Stack.Navigator>
    );
}

export default ProfileStack