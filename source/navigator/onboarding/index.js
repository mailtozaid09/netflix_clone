import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import OnBoardingScreen from '../../screens/onboarding';
import Header from '../../components/header/onboarding';
import { colors } from '../../global/colors';
import WhoIsWatching from '../../screens/dashboard/watching';
import WhoIsWatchingHeader from '../../components/header/watching/WhoIsWatchingHeader';
import EditWatching from '../../screens/dashboard/watching/EditWatching';
import EditWatchingHeader from '../../components/header/watching/EditWatchingHeader';
import AddWatching from '../../screens/dashboard/watching/AddWatching';



const Stack = createStackNavigator();


const OnboardingStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="OnBoarding" 
        >
            <Stack.Screen
                name="OnBoarding"
                component={OnBoardingScreen}
                options={({navigation}) => ({
                    headerShown: true,
                    headerStyle: styles.headerStyle,
                    headerTitle: '',
                    headerTitle: () => (
                        <Header navigation={navigation} />
                    ),
                })}
            />
            <Stack.Screen
                name="WhoIsWatching"
                component={WhoIsWatching}
                options={({navigation}) => ({
                    headerShown: false,
                    headerTitle: '',
                    headerStyle: [styles.headerStyle, {backgroundColor: colors.black}],
                    headerLeft: () => null,
                    headerTitle: () => <WhoIsWatchingHeader navigation={navigation} />,
                })}
            />
            <Stack.Screen
                name="EditWatching"
                component={EditWatching}
                options={({navigation}) => ({
                    headerShown: false,
                    headerTitle: '',
                })}
            />
            <Stack.Screen
                name="AddWatching"
                component={AddWatching}
                options={({navigation}) => ({
                    headerShown: false,
                    headerTitle: '',
                })}
            />
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    headerStyle: {
        backgroundColor: colors.black,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0,
    }
})

export default OnboardingStack