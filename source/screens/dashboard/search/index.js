import React,{useState, useEffect} from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet, SafeAreaView, ScrollView } from 'react-native'
import SearchInput from '../../../components/input/SearchInput'
import { movies } from '../../../global/sampleData';
import { colors } from '../../../global/colors';
import { InstagramSans } from '../../../global/fontFamily';
import { media } from '../../../global/media';
import { getAllMovies } from '../../../global/api';


const SearchScreen = ({navigation}) => {


    const [searchValue, setSearchValue] = useState('');

    const [originalData, setOriginalData] = useState([]);
    const [moviesData, setMoviesData] = useState([]);

    useEffect(() => {
        
        getAllMoviesFunction()
     
    }, [])
    

    const getAllMoviesFunction = () => {
        getAllMovies()
        .then((resp) => {
            setOriginalData(resp.results)
            setMoviesData(resp.results)
            console.log("resp search => ", resp.results);
        })
        .catch((err) => {
            console.log("err search => ", err);
        })
    }

    const searchFilter = (text) => {
        setSearchValue(text)
        var result = search(originalData, text)

        if(result){
            setMoviesData(result)
        }else{
            setMoviesData(originalData)
        }
    }

    const search = (arr, str) => {
        const chars = str.split('');
        return arr.filter(
            item => chars.every(char => item.original_title.includes(char)) 
        );
    }

        

    return (
        <SafeAreaView style={styles.container} >
            <SearchInput 
                placeholder="Search"
                searchValue={searchValue}
                onChangeText={(text) => searchFilter(text)}
                clearInputValue={() => setSearchValue('')}
            />

            <ScrollView>
                <Text style={styles.topSearches} >Top {searchValue ? 'results' : 'searches'}</Text>
        
                {moviesData?.slice(0, 10).map((movie, index) => (
                    <TouchableOpacity
                        key={index}
                        onPress={() => navigation.navigate('MoviesDetails', { movie })}
                        style={{backgroundColor: '#211f1f', marginBottom: 8, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} 
                    >
                        <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}} >
                            <Image 
                                source={{
                                    uri: `https://image.tmdb.org/t/p/original${movie?.poster_path}`
                                }}
                                style={{height: 80, width: 150, marginRight: 8}} 
                            />
                            <View style={{flex: 1, marginRight: 10}} >
                                <Text numberOfLines={1} style={styles.movieTitle} >{movie.original_title}</Text>
                            </View>
                        </View>
                        <Image source={media.play_circle} style={{height: 30, width: 30, marginRight: 20}} />
                    </TouchableOpacity>
                ))}
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.black,
    },
    topSearches: {
        fontSize: 20,
        color: colors.white,
        marginBottom: 20,
        marginLeft: 12,
        fontFamily: InstagramSans.Bold
    },
    movieTitle: {
        fontSize: 18,
        color: colors.white,
        fontFamily: InstagramSans.Medium
    }
})

export default SearchScreen