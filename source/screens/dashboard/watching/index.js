import React,{useState, useEffect} from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet, SafeAreaView, ScrollView } from 'react-native'

import { colors } from '../../../global/colors';
import { media } from '../../../global/media';

import { InstagramSans } from '../../../global/fontFamily';

import { getAllMovies } from '../../../global/api';
import WhoIsWatchingHeader from '../../../components/header/watching/WhoIsWatchingHeader';
import { currentProfile } from '../../../store/modules/profile/actions';
import { useDispatch, useSelector } from 'react-redux';
import { screenLoader } from '../../../store/modules/home/actions';

const profiles = [
    // {
    //     id: 1,
    //     name: 'Zaid',
    //     icon: media.profile1
    // },
    // {
    //     id: 2,
    //     name: 'Children',
    //     icon: media.kids
    // },
    
]


const WhoIsWatching = ({navigation}) => {

    const dispatch = useDispatch()

    const [editMode, setEditMode] = useState(false);

    const profile_list = useSelector(state => state.profile.profile_list);

    //console.log("current_profile ", current_profile);


    useEffect(() => {
        console.log("profile_list ");
    }, [profile_list])
    

    const setCurrentProfile = (profile) => {
        dispatch(currentProfile(profile))
        dispatch(screenLoader(true))
        navigation.navigate('Tabbar')

        setTimeout(() => {
            dispatch(screenLoader(false))
        }, 1000);
    }
    



    return (
        <SafeAreaView style={styles.container} >
                <WhoIsWatchingHeader 
                    editMode={editMode} 
                    navigation={navigation} 
                    onEditFunction={() => setEditMode(!editMode)}
                />

                <View style={{ justifyContent: 'space-evenly', flexDirection: 'row', flexWrap: 'wrap', padding: 50, }} >
                    {profile_list?.map((item, index) => (
                        <View key={index} style={{alignItems: 'center', margin: 20, marginBottom: 0}} >
                            <TouchableOpacity
                                activeOpacity={0.3 }
                                disabled={editMode ? true : false}
                                onPress={() => {setCurrentProfile(item)}}
                            >
                                <Image source={item.user_icon} style={{height: 100, width: 100, borderRadius: 10}} />
                            </TouchableOpacity>
                            {editMode && <TouchableOpacity 
                                activeOpacity={0.3}
                                onPress={() => {navigation.navigate('EditWatching', {item}); setEditMode(!editMode); }}
                                style={{position: 'absolute', alignItems: 'center', justifyContent: 'center', height: 100, width: 100, backgroundColor: '#00000090'}} >
                                <Image source={media.edit} style={{height: 40, width: 40, }} />
                            </TouchableOpacity>}
                            <Text style={styles.profileTitle} >{item.user_name}</Text>
                        </View>
                    ))}
                    <View style={{alignItems: 'center', margin: 20, marginBottom: 0}} >
                        <TouchableOpacity 
                             activeOpacity={0.3}
                             onPress={() => {navigation.navigate('AddWatching');}}
                             style={{height: 100, width: 100, borderWidth: 1, borderColor: colors.grey, alignItems: 'center', justifyContent: 'center', borderRadius: 10}}>
                            <Image source={media.plus} style={{height: 30, width: 30, }} />
                        </TouchableOpacity>
                        <Text style={styles.profileTitle} >Add Profile</Text>
                    </View>
                </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.black,
    },
    profileTitle: {
        fontSize: 18,
        color: colors.white,
        marginTop: 10,
        fontFamily: InstagramSans.Medium,
    },
})

export default WhoIsWatching