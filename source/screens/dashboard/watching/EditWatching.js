import React,{useState, useEffect} from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet, SafeAreaView, ScrollView, TextInput } from 'react-native'

import { colors } from '../../../global/colors';
import { media } from '../../../global/media';

import { InstagramSans } from '../../../global/fontFamily';

import { getAllMovies } from '../../../global/api';
import EditWatchingHeader from '../../../components/header/watching/EditWatchingHeader';
import { useDispatch } from 'react-redux';

import { deleteUserProfile, updateUserProfile } from '../../../store/modules/profile/actions';

const services = [
    {
        title: 'Game handle',
        subTitle: 'Create your game handle',
        icon: media.gamehandle,
        isAuto: false,
    },
    {
        title: 'Maturity rating',
        subTitle: 'No restrictions',
        icon: media.maturity,
        isAuto: false,
    },
    {
        title: 'Display language',
        subTitle: 'English',
        icon: media.language,
        isAuto: false,
    },
    {
        title: 'Audio & Subtitles',
        subTitle: 'English and English ( India )',
        icon: media.subtitle,
        isAuto: false,
    },
    {
        title: 'Autoplay next episode',
        subTitle: null,
        icon: media.autoplay,
        isAuto: false,
    },
    {
        title: 'Autoplay previews',
        subTitle: null,
        icon: media.autopreview,
        isAuto: false,
    },
]

const EditWatching = (props) => {

    const dispatch = useDispatch()


    const navigation = props.navigation

    const [user_details, setUser_details] = useState(props?.route?.params?.item);
    const [inputValue, setInputValue] = useState(props.route.params.item.user_name);

    useEffect(() => {
       
     console.log("parmas edit => ", props.route.params.item);
    }, [])

    const onSaveFunction = () => {
        dispatch(updateUserProfile({
            id: user_details?.id,
            user_name: inputValue,
            user_icon: user_details?.user_icon,
        }))

        navigation.goBack()
    }


    const onDelteFunction = () => {
        dispatch(deleteUserProfile(user_details?.id))

        navigation.goBack()
    }

    return (
        <SafeAreaView style={styles.container} >
            <EditWatchingHeader
                navigation={navigation} 
                onCancelFunction={() => {navigation.goBack()}}
                onSaveFunction={() => {onSaveFunction()}}
            />

          <ScrollView style={{width: '100%', }} contentContainerStyle={{alignItems: 'center'}} >
                <View style={{marginBottom: 40}} >
                    <Image source={user_details?.user_icon} style={{height: 100, width: 100,  }} />
                    <View style={{position: 'absolute', bottom: -15, right: -15}} >
                        <TouchableOpacity 
                            activeOpacity={0.3}
                            //onPress={() => {navigation.navigate('AddWatching');}}
                            style={{height: 40, width: 40, borderRadius: 20, backgroundColor: colors.accent, alignItems: 'center', justifyContent: 'center', }}>
                        <Image source={media.edit} style={{height: 20, width: 20, }} />
                    </TouchableOpacity>
                    </View>
                </View>
            
                <View style={{paddingHorizontal: 40, width: '100%',}} >
                    <Text style={styles.label}>Profile Name</Text>             
                    <TextInput
                        placeholder="Profile Name"
                        placeholderTextColor={colors.grey}
                        style={styles.inputStyle}
                        value={inputValue}
                        onChangeText={(text) => setInputValue(text)}
                    />       
                    
                </View>


                <View style={{width: '100%', marginTop: 40,}} >
                    {services?.map((item, index) => (
                        <View
                            key={index}
                            style={{height: 65,  backgroundColor: colors.accent, marginHorizontal: 12, padding: 12, borderRadius: 8, marginBottom: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', }}
                        >
                            <Image source={item.icon} style={{height: 28, width: 28, resizeMode: 'contain', marginRight: 20 }} />
                            <View style={{flex: 1}} >
                                <Text style={styles.title} >{item.title}</Text>
                                {item.subTitle && <Text style={styles.subtitle} >{item.subTitle}</Text>}
                            </View>
                            <Image source={media.right_arrow} style={{height: 18, width: 18, resizeMode: 'contain', marginLeft: 20 }} />
                        </View>

                    ))}
                </View>

                <View style={{alignItems: 'center', marginBottom: 30}} >
                    <Text style={styles.childrenSubTitle} >Changes made here apply to all devices.</Text>
                    <TouchableOpacity 
                        onPress={() => {onDelteFunction()}}
                        style={{flexDirection: 'row', alignItems: 'center'}} >
                        <Image source={media.delete} style={{height: 32, width: 32, marginRight: 20}} />
                        <Text style={styles.childrenTitle} >Delete Profile</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.black,
    },
    inputStyle: {
        borderWidth: 1,
        borderRadius: 8,
        borderColor: colors.grey,
        height: 60,
        width: '100%',
        paddingLeft: 20,
        fontSize: 18,
        color: colors.white,
        fontFamily: InstagramSans.Medium
    },
    childrenTitle: {
        fontSize: 22,
        color: colors.white,
       
        textAlign: 'center',
        fontFamily: InstagramSans.Medium
    },
    childrenSubTitle: {
        fontSize: 18,
        color: colors.grey,
        textAlign: 'center',
        marginVertical: 30,
        fontFamily: InstagramSans.Medium
    },
    toggleContainer: {
        marginVertical: 30,
    },
    title: {
        fontSize: 20,
        color: colors.white,
        fontFamily: InstagramSans.Medium,
    },
    subtitle: {
        fontSize: 16,
        color: colors.grey,
        fontFamily: InstagramSans.Medium,
    },
    label: {
        fontSize: 16,
        color: colors.white,
        marginBottom: 10,
        fontFamily: InstagramSans.Regular,
    }
})

export default EditWatching