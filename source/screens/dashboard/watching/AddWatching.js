import React,{useState, useEffect} from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet, SafeAreaView, ScrollView, TextInput } from 'react-native'

import { colors } from '../../../global/colors';
import { media } from '../../../global/media';

import { InstagramSans } from '../../../global/fontFamily';

import { getAllMovies } from '../../../global/api';
import AddWatchingHeader from '../../../components/header/watching/AddWatchingHeader';
import { useDispatch } from 'react-redux';
import { addUserProfile } from '../../../store/modules/profile/actions';


const AddWatching = ({navigation}) => {

    const dispatch = useDispatch()

    const [inputValue, setInputValue] = useState('');

    const onSaveFunction = () => {

        console.log("inputValue => ", inputValue);
        dispatch(addUserProfile({
            user_name: inputValue,
            user_icon: media.profile1,
        }))

        navigation.goBack()

    }

    return (
        <SafeAreaView style={styles.container} >
            <AddWatchingHeader
                inputValue={inputValue}
                navigation={navigation} 
                onCancelFunction={() => {navigation.goBack()}}
                onSaveFunction={() => {onSaveFunction()}}
            />

            <View style={{marginBottom: 40}} >
                <Image source={media.profile1} style={{height: 100, width: 100,  }} />
                <View style={{position: 'absolute', bottom: -15, right: -15}} >
                    <TouchableOpacity 
                        activeOpacity={0.3}
                        //onPress={() => {navigation.navigate('AddWatching');}}
                        style={{height: 40, width: 40, borderRadius: 20, backgroundColor: colors.accent, alignItems: 'center', justifyContent: 'center', }}>
                    <Image source={media.edit} style={{height: 20, width: 20, }} />
                </TouchableOpacity>
                </View>
            </View>
        
            <View style={{paddingHorizontal: 40, width: '100%', }} >
                <Text style={styles.label}>Profile Name</Text>             
                <TextInput
                    placeholder="Profile Name"
                    placeholderTextColor={colors.grey}
                    style={styles.inputStyle}
                    onChangeText={(text) => setInputValue(text)}
                />

                <View style={styles.toggleContainer} >
                    <Text>toggle</Text>
                </View>

                <Text style={styles.childrenTitle} >Children's Profile</Text>
                <Text style={styles.childrenSubTitle} >Made for children 12 and under, but parents have all the control</Text>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.black,
    },
    inputStyle: {
        borderWidth: 1,
        borderRadius: 8,
        borderColor: colors.grey,
        height: 60,
        width: '100%',
        paddingLeft: 20,
        fontSize: 18,
        color: colors.white,
        fontFamily: InstagramSans.Medium
    },
    childrenTitle: {
        fontSize: 22,
        color: colors.white,
        marginBottom: 8,
        textAlign: 'center',
        fontFamily: InstagramSans.Medium
    },
    childrenSubTitle: {
        fontSize: 18,
        color: colors.grey,
        textAlign: 'center',
        fontFamily: InstagramSans.Medium
    },
    toggleContainer: {
        marginVertical: 30,
    },
    label: {
        fontSize: 16,
        color: colors.white,
        marginBottom: 10,
        fontFamily: InstagramSans.Regular,
    }
})

export default AddWatching