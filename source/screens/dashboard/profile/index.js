import React,{useState, useEffect} from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet, SafeAreaView, ScrollView, TextInput } from 'react-native'
import EditWatchingHeader from '../../../components/header/watching/EditWatchingHeader'
import { useDispatch, useSelector } from 'react-redux'
import { signOut } from '../../../store/modules/auth/actions'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { media } from '../../../global/media'
import { colors } from '../../../global/colors'
import { InstagramSans } from '../../../global/fontFamily'
import { color } from 'react-native-reanimated'


const options = [
    {
        title: 'My List',
        icon: media.mylist,
    },
    {
        title: 'App Settings',
        icon: media.settings,
    },
    {
        title: 'Account',
        icon: media.account,
    },
    {
        title: 'Help',
        icon: media.help,
    },
]

const ProfileSettings = ({navigation}) => {

    
    const dispatch = useDispatch()

    const profile_list = useSelector(state => state.profile.profile_list);


    const onLogoutFunction = () => {
        dispatch(signOut())
        AsyncStorage.clear()
    }

    return (
        <SafeAreaView style={styles.container} >

            <TouchableOpacity
                onPress={() => onLogoutFunction()}
            >
                <Text>ProfileSettings</Text>
            </TouchableOpacity>

            <View style={{marginBottom: 20, flexDirection: 'row', justifyContent: 'space-evenly', width: '100%'}} >
                {profile_list?.map((profile) => (
                    <View style={{alignItems: 'center', width: 60, }} >
                        <Image source={profile.user_icon} style={{height: 50, width: 50, borderRadius: 8,}} />
                        <Text numberOfLines={1} style={styles.profileName} >{profile?.user_name}</Text>
                    </View>
                ))}
                <View style={{alignItems: 'center', width: 60, }} >
                    <View style={{height: 50, width: 50, borderRadius: 8, alignItems: 'center', justifyContent: 'center', borderWidth: 1, borderColor: colors.grey}}  >
                        <Image source={media.plus} style={{height: 30, width: 30, }} />
                    </View>
                    <Text numberOfLines={1} style={styles.profileName} >Add Profile</Text>
                </View>
            </View>


            <TouchableOpacity 
                onPress={() => {}}
                style={{flexDirection: 'row', alignItems: 'center'}} >
                <Image source={media.edit} style={{height: 20, width: 20, marginRight: 20}} />
                <Text style={[styles.childrenTitle, {color: colors.white}]} >Manage Profiles</Text>
            </TouchableOpacity>

            <View style={{width: '100%', marginTop: 40,}} >
                {options?.map((item, index) => (
                    <View
                        key={index}
                        style={{height: 55,  backgroundColor: colors.accent, padding: 12, marginBottom: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', }}
                    >
                        <Image source={item.icon} style={{height: 28, width: 28, resizeMode: 'contain', marginRight: 20 }} />
                        <View style={{flex: 1}} >
                            <Text style={styles.title} >{item.title}</Text>
                        </View>
                        <Image source={media.right_arrow} style={{height: 18, width: 18, resizeMode: 'contain', marginLeft: 20 }} />
                    </View>

                ))}
            </View>

            <View style={{alignItems: 'center', marginVertical: 20}} >
                    <TouchableOpacity 
                        onPress={() => {onLogoutFunction()}}
                        style={{flexDirection: 'row', alignItems: 'center'}} >
                        <Text style={styles.childrenTitle} >Sign Out</Text>
                    </TouchableOpacity>

                    <Text style={styles.childrenSubTitle} >Version: 15.21.1 (18) 5.0.1 - 001</Text>
                    
                </View>


        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.black,
    },
    title: {
        fontSize: 20,
        color: '#B3B3B3',
        fontFamily: InstagramSans.Medium,
    },
    profileName: {
        fontSize: 14,
        color: '#B3B3B3',
        marginTop: 8,
        fontFamily: InstagramSans.Medium,
    },
    childrenTitle: {
        fontSize: 22,
        color: colors.grey,
        textAlign: 'center',
        fontFamily: InstagramSans.Medium
    },
    childrenSubTitle: {
        fontSize: 18,
        color: colors.grey,
        textAlign: 'center',
        marginVertical: 10,
        fontFamily: InstagramSans.Light
    },
})

export default ProfileSettings