import React, {useState, useEffect} from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, SafeAreaView, FlatList } from 'react-native'
import { getAllGenreList, getDayFromDate, getMonthDayFromDate, getMonthFromDate, getPopularMovies, getTopRatedMovies, getUpcomingMovies } from '../../../global/api'
import { ScrollView } from 'react-native-gesture-handler'
import { InstagramSans } from '../../../global/fontFamily'
import { colors } from '../../../global/colors'
import { media } from '../../../global/media'
import { newsAndHotTabs } from '../../../global/sampleData'

// const upcomingMovies = [
//     {
//         "adult": false,
//         "backdrop_path": "/svd6fPPsloJKbS72PJJ7xJqTevZ.jpg",
//         "genre_ids": [
//           Array
//         ],
//         "id": 942199,
//         "original_language": "en",
//         "original_title": "Simulant",
//         "overview": "Faye attempts to replace her newly deceased husband, Evan, with an android simulant (SIM). Although SIM Evan appears like human Evan in every way, Faye does not feel the same love for SIM Evan as it does for her. SIM Evan tries to win Faye back while at the same time being on-the-run from a government agent chasing down SIMs who have become “conscious” and could potentially be a threat to humankind.",
//         "popularity": 164.798,
//         "poster_path": "/kmDJhZIq1xnu5ZiMOlb0nHJH1qb.jpg",
//         "release_date": "2023-03-29",
//         "title": "Simulant",
//         "video": false,
//         "vote_average": 6.2,
//         "vote_count": 49
//       },
//     {
//       "adult": false,
//       "backdrop_path": "/eTvN54pd83TrSEOz6wbsXEJktCV.jpg",

//       "id": 882569,
//       "original_language": "en",
//       "original_title": "Guy Ritchie's The Covenant",
//       "overview": "During the war in Afghanistan, a local interpreter risks his own life to carry an injured sergeant across miles of grueling terrain.",
//       "popularity": 2243.161,
//       "poster_path": "/aX0H63vho7rZ9Rm3I567Zf00Z1t.jpg",
//       "release_date": "2023-04-19",
//       "title": "Guy Ritchie's The Covenant",
//       "video": false,
//       "vote_average": 7.8,
//       "vote_count": 204
//     },
// ]

const NewsAndHotScreen = ({navigation}) => {

    const [genreList, setGenreList] = useState([]);
    const [genreNameList, setGenreNameList] = useState([]);

    const [upcomingMovies, setUpcomingMovies] = useState([]);
    const [popularMovies, setPopularMovies] = useState([]);
    const [topRatedMovies, setTopRatedMovies] = useState([]);

    const [moviesData, setMoviesData] = useState([]);

    const [currentTab, setCurrentTab] = useState(0);

    useEffect(() => {
        getUpcomingMoviesFunction()
        getPopularMoviesFunction()
        getTopRatedMoviesFunction()
    
        getAllGenreListFunction()
    }, [])



    const getAllGenreListFunction = () => {
        getAllGenreList()
        .then((resp) => {
            //console.log("genre= >>> ", resp.genres);
            setGenreList(resp.genres)
        })
        .catch((err) => {
            console.log("genre= err !!! >>> ", err);
        })
    }

    const getUpcomingMoviesFunction = () => {
        getUpcomingMovies()
        .then((resp) => {
            //console.log("upcoming= >>> ", resp.results);
            setUpcomingMovies(resp.results)
            setMoviesData(resp.results)
        })
        .catch((err) => {
            console.log("upcoming= err !!! >>> ", err);
        })
    }

    const getPopularMoviesFunction = () => {
        getPopularMovies()
        .then((resp) => {
            //console.log("popular= >>> ", resp.results);
            setPopularMovies(resp.results)
        })
        .catch((err) => {
            console.log("popular= err !!! >>> ", err);
        })
    }

    const getTopRatedMoviesFunction = () => {
        getTopRatedMovies()
        .then((resp) => {
            //console.log("TopRated= >>> ", resp.results);
            setTopRatedMovies(resp.results)
        })
        .catch((err) => {
            console.log("TopRated= err !!! >>> ", err);
        })
    }
    
    const getMovieGenre = (genre) => {
        var genreName  = []
        genreList.forEach((item) => {
            genre.includes(item.id) ? genreName.push(item.name) : null
        })
        return genreName
    } 

    const onChangeTab = (index) => {
        setCurrentTab(index)
        
        if(index == 0){
            setMoviesData(upcomingMovies)
        }else if(index == 1){
            setMoviesData(popularMovies)
        }else{
            setMoviesData(topRatedMovies)
        }
    }

    return (
        <SafeAreaView style={styles.container} >
   
            <ScrollView
                horizontal
            >
                {newsAndHotTabs.map((item, index) => (
                    <TouchableOpacity 
                        key={index}
                        onPress={() => onChangeTab(index)}
                        style={[{flexDirection: 'row', alignItems: 'center', margin: 12, marginVertical: 20, height: 40, paddingHorizontal: 10, borderRadius: 25,}, currentTab == index ? {backgroundColor: colors.white, } : null]} >
                        <Image source={item.image} style={{resizeMode: 'contain', height: 20, width: 20, marginRight: 10, }} />
                        <Text style={[styles.tabTitle, {color: currentTab == index ? colors.black : colors.white} ]} >{item.title}</Text>
                    </TouchableOpacity>
                ))}
            </ScrollView>

            <ScrollView>
            
                <View>
                    {moviesData?.map((movie, index) => (
                        <View
                            key={index}
                            style={{flexDirection: 'row', padding: 12 }} >
                            <View style={{backgroundColor: colors.black, width: 44, alignItems: 'center'}} >
                                <Text style={styles.dateMonth} >{getMonthFromDate(movie.release_date)}</Text>
                                <Text style={styles.dateDay} >{getDayFromDate(movie.release_date)}</Text>
                            </View>
                            <View style={{backgroundColor: colors.black, flex: 1, marginLeft: 10}} >
                                <Image
                                    style={{height: 180, width: '100%', borderRadius: 20, }}
                                    source={{
                                        uri: `https://image.tmdb.org/t/p/original${movie.backdrop_path}`
                                    }}
                                />

                                <View style={{flexDirection: 'row', alignItems: 'center', marginVertical: 20, marginBottom: 0 }} >
                                    <TouchableOpacity
                                        style={{alignItems: 'center', marginRight: 15,}}
                                    >
                                        <Image source={media.remind} style={{height: 30, width: 30, marginBottom: 5, resizeMode: 'contain'}} />
                                        <Text style={styles.moviedesc} >Remind me</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        style={{alignItems: 'center', marginRight: 15,}}
                                    >
                                        <Image source={media.info} style={{height: 30, width: 30, marginBottom: 5, resizeMode: 'contain'}} />
                                        <Text style={styles.moviedesc} >Info</Text>
                                    </TouchableOpacity>
                                </View>

                                {currentTab == 0 && <Text style={styles.commingOn} >Coming on {getDayFromDate(movie.release_date)} {getMonthFromDate(movie.release_date)}</Text>}
                                    
                                <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 20}}>
                                    <Image source={media.netflixIcon} style={{height: 20, width: 20, resizeMode: 'contain'}} />
                                    <Text style={[styles.moviedesc, {marginBottom: 0, marginLeft: 6, fontSize: 16}]} >FILM</Text>
                                </View>

                                <Text style={styles.movietitle} >{movie.title}</Text>
                                <Text style={styles.moviedesc}  numberOfLines={4} >{movie.overview}</Text>
                                <View style={{flexDirection: 'row', alignItems: 'center', flexWrap: 'wrap'}} >
                                    {getMovieGenre(movie.genre_ids)?.map((item, index) => (
                                        <View key={index} style={{flexDirection: 'row', alignItems: 'center'}} >
                                            <Text style={styles.genreName} >{item}</Text>
                                            {getMovieGenre(movie.genre_ids)?.length -1 > index ? <Text style={styles.dotStyle} >{'\u2B24'}</Text> : null}
                                        </View>
                                    ))}
                                </View>
                            </View>
                        </View>
                    ))}
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.black
    },
    title: {
        fontSize: 16,
        color: colors.black,
        marginBottom: 8,
        fontFamily: InstagramSans.Bold
    },
    commingOn: {
        fontSize: 18,
        color: colors.white,
        marginBottom: 8,
        fontFamily: InstagramSans.Medium
    },
    movietitle: {
        fontSize: 20,
        color: colors.white,
        marginBottom: 8,
        fontFamily: InstagramSans.Bold
    },
    moviedesc: {
        fontSize: 18,
        color: colors.grey,
        marginBottom: 8,
        fontFamily: InstagramSans.Regular
    },
    genreName: {
        fontSize: 16,
        color: colors.white,
        marginBottom: 8,
        fontFamily: InstagramSans.Regular
    },
    dotStyle: {
        color: colors.white,
        fontSize: 6,
        marginHorizontal: 8,
        marginBottom: 5
    },
    dateDay: {
        fontSize: 34,
        color: colors.white,
        marginBottom: 8,
        fontFamily: InstagramSans.Regular
    },
    dateMonth: {
        fontSize: 22,
        color: colors.grey,
        fontFamily: InstagramSans.Regular
    },
    tabTitle: {
        fontSize: 18,
        fontFamily: InstagramSans.Medium
    }
})

export default NewsAndHotScreen