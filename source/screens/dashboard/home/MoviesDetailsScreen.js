import React, {useState, useEffect} from 'react';
import { Text, View, TouchableOpacity, StyleSheet, Image } from 'react-native';

import PropTypes from 'prop-types';

import { Container, Title, ScrollView, Cover, Icon, Series, SeriesTitle, MovieTitle, MovieContents, MovieDescription, MovieCast } from './styles';
import { media } from '../../../global/media';
import PlayButton from '../../../components/button/PlayButton';


import { screenWidth } from '../../../global/constants';
import { getMonthDayYearFromDate, getSimilarMovies } from '../../../global/api';
import { colors } from '../../../global/colors';
import VideoComponent from '../../../components/video';

import CloseIcon from 'react-native-vector-icons/AntDesign';

const MoviesDetailsScreen = (props) => {

    const [movieDetails, setMovieDetails] = useState(props.route.params.movie);

    const [similarMovies, setSimilarMovies] = useState([]);

    const [currentTab, setCurrentTab] = useState('moreLikeThis');

    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', () => {

            getSimilarMoviesFunc()

          });

          return unsubscribe;
    }, [props.navigation]);

    const getSimilarMoviesFunc = (movie) => {
        setMovieDetails(movie ? movie : props.route.params.movie)
        getSimilarMovies(movie ? movie.id : props.route.params.movie.id)
            .then((resp) => {
                setSimilarMovies(resp.results)
            })
            .then((err) => {
                console.log("err => ", err);
            })
    }


    var series = "SERIES"

    var movieTitle = movieDetails?.original_title
    var movieDescription = movieDetails?.overview
    var movieCast = movieDetails?.original_title
    var movieDirector = movieDetails?.original_title
    var movieYear = movieDetails?.release_date
    var movieDuration = "2h 39m"

    return (
        <Container>
            
            
            <VideoComponent />

            <View style={{position: 'absolute', width: '100%', top: 60 }}>
                <View style={{alignItems: 'flex-end', margin: 12, marginBottom: 10, marginTop: 0}} >
                    <TouchableOpacity
                            onPress={() => {props.navigation.goBack()}}
                            style={{backgroundColor: colors.accent, height: 40, width: 40, borderRadius: 20, alignItems: 'center', justifyContent: 'center'}}
                        >
                        <CloseIcon name="close" color={colors.white} size={26}  />
                    </TouchableOpacity>
                </View>
            </View>

            <ScrollView>
                
                <Series>
                    <Icon source={media.netflixIcon} />
                    <SeriesTitle>{series}</SeriesTitle>
                </Series>

                <MovieTitle>{movieTitle}</MovieTitle>
                

                <MovieContents>{getMonthDayYearFromDate(movieYear)}, {movieDuration} </MovieContents>

                <PlayButton 
                    title="Play" 
                    icon={media.play} 
                    onPress={() => {props.navigation.navigate('ViewVideo')}}
                />

                <MovieDescription numberOfLines={3} >{movieDescription}</MovieDescription>
                {/* <MovieCast>{movieCast}</MovieCast>
                <MovieCast>{movieDirector}</MovieCast> */}

                <View style={styles.iconContainer} >
                    <TouchableOpacity style={{alignItems: 'center', justifyContent: 'center', width: 100,}} >
                        <Image source={media.plus} style={{height: 30, width: 30, marginBottom: 8}} />
                        <Text style={{fontSize: 14, color: colors.grey}}>My List</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{alignItems: 'center', justifyContent: 'center', width: 80,}} >
                        <Image source={media.like} style={{height: 30, width: 30, marginBottom: 8}} />
                        <Text style={{fontSize: 14, color: colors.grey}}>Rate</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={{alignItems: 'center', justifyContent: 'center', width: 80,}} >
                        <Image source={media.share} style={{height: 30, width: 30, marginBottom: 8}} />
                        <Text style={{fontSize: 14, color: colors.grey}}>Share</Text>
                    </TouchableOpacity>
                </View>

                <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 10}} >
                    <TouchableOpacity
                        onPress={() => {setCurrentTab('moreLikeThis')}}
                        style={[styles.tabStyle, currentTab == 'moreLikeThis' ? {borderColor: colors.primary, borderTopWidth: 6, } : {borderColor: colors.accent, borderTopWidth: 6,}]}
                    >
                        <Text style={{fontSize: 20, fontWeight: 'bold', color: currentTab == 'moreLikeThis' ? colors.white : colors.grey}}>More Like This</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => {setCurrentTab('trailers')}}
                        style={[styles.tabStyle, currentTab != 'moreLikeThis' ? {borderColor: colors.primary, borderTopWidth: 6, } : {borderColor: colors.accent, borderTopWidth: 6,}]}
                    >
                        <Text style={{fontSize: 20, fontWeight: 'bold', color:  currentTab != 'moreLikeThis' ? colors.white : colors.grey}}>Trailers & more</Text>
                    </TouchableOpacity>
                </View>

                {currentTab == 'moreLikeThis'
                    ?
                    <View style={{flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'space-between' }} >
                        {similarMovies?.slice(0, 12).map((movie, index) => (
                            <TouchableOpacity
                            key={index}
                            onPress={() => getSimilarMoviesFunc(movie)}
                            >
                                <Image
                                style={{height: 160, width: screenWidth/3-20, marginBottom: 15, borderRadius: 8, }}
                                source={{
                                    uri: `https://image.tmdb.org/t/p/original${movie?.poster_path}`
                                }}
                                />
                            </TouchableOpacity>
                        ))}
                    </View>
                    :
                    <View>
                        <Image
                        style={{height: 200, width: '100%', paddingHorizontal: 12,  borderRadius: 8, resizeMode: 'contain'}}
                        source={{
                            uri: `https://image.tmdb.org/t/p/original${movieDetails?.backdrop_path}`
                        }}
                        />
                    </View>
                }

                

            </ScrollView>
        </Container>
    )
}

const styles = StyleSheet.create({
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 20,
    },
    tabStyle: {
        height: 60, 
        alignItems: 'center', 
        justifyContent: 'center', 
        marginRight: 20,
    },
})

export default MoviesDetailsScreen;
