// import React, {useState, useEffect} from 'react'
// import { Text, View, Image, StyleSheet, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native'

// import styled from 'styled-components/native';
// import { colors } from '../../../global/colors';
// import Header from '../../../components/header/home';
// import Banner from '../../../components/banner';
// import { getActionMovies, getComedyMovies, getHorrorMovies } from '../../../global/api';
// import { useSelector } from 'react-redux';
// import MoviesList from '../../../components/movielist';

// const HomeScreen = ({navigation}) => {

//     const [storedMovies, setStoredMovies] = useState([]);

//     const [allMovies, setAllMovies] = useState([]);
  

//     const [actionMovies, setActionMovies] = useState([]);
//     const [comedyMovies, setComedyMovies] = useState([]);
//     const [horrorMovies, setHorrorMovies] = useState([]);

//     const user = useSelector(state => state.auth.user);
  
//     // useEffect(() => {

//     //   async function loadUserMovies() {
//     //     const stored = JSON.parse(await AsyncStorage.getItem(user.email));
  
//     //     if (stored) {
//     //       const filtered = movies.filter(movie => stored.includes(movie.id));
//     //       setStoredMovies(filtered);
//     //     }
//     //   }
  
//     //   loadUserMovies();
//     // }, [isFocused]);

//     useEffect(() => {
//         getActionMovies()
//         .then((resp) => {
//             //console.log("RESPONSE => ", resp);
//             setActionMovies(resp.results)
        
//             const arr1 = resp.results;
//             const arr2 = allMovies;
//             const children = arr1.concat(arr2);
//             setAllMovies(children)
//         })
//         .catch((err) => {
//             console.log("ERROR ! ! ! => ", err);
//         })

//         getComedyMovies()
//         .then((resp) => {
//             //console.log("RESPONSE => ", resp);
//             setComedyMovies(resp.results)
//             const arr1 = resp.results;
//             const arr2 = allMovies;
//             const children = arr1.concat(arr2);
//             setAllMovies(children)
//         })
//         .catch((err) => {
//             console.log("ERROR ! ! ! => ", err);
//         })

//         getHorrorMovies()
//         .then((resp) => {
//             //console.log("RESPONSE => ", resp);
//             setHorrorMovies(resp.results)
//             const arr1 = resp.results;
//             const arr2 = allMovies;
//             const children = arr1.concat(arr2);
//             setAllMovies(children)
//         })
//         .catch((err) => {
//             console.log("ERROR ! ! ! => ", err);
//         })


//     }, [])
    


//     return (
//         <SafeAreaView style={{flex: 1, }} >

//             <Container>
//                 <Header navigation={navigation} />

//                 <ScrollView style={{padding: 12}} >
//                     <Banner />

//                     <MoviesList title="Action" movies={actionMovies} navigation={navigation} />
//                     <MoviesList title="Comedy" movies={comedyMovies} navigation={navigation} />
//                     <MoviesList title="Horror" movies={horrorMovies} navigation={navigation} />

//                 </ScrollView>
//             </Container>
//         </SafeAreaView>
//     )
// }

// const Container = styled(SafeAreaView).attrs({
//     forceInset: { bottom: 'never' },
//   })`
//     background-color: ${colors.black};
//     flex: 1;
//     padding: 12px;
// `;

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,

//     }
// })

// export default HomeScreen