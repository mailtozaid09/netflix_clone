import React, {useState, useEffect} from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, SafeAreaView } from 'react-native'

import { colors } from '../../../global/colors';

import { InstagramSans } from '../../../global/fontFamily';
import ToastMessage from '../../../components/toast';



const AccountCreated = ({navigation}) => {

    var userEmail = 'maltozaid09@gmail.com'

    useEffect(() => {
        setTimeout(() => {
            navigation.navigate('LoginStack', {screen: 'Login'})
        }, 1000);
    }, [])
    

    return (
        <SafeAreaView style={styles.container} >
            <View style={{width: '100%', padding: 20, paddingHorizontal: 30}} >
                
                <ToastMessage
                    title="Account created"
                />

                <Text style={styles.title} >Check your email to learn more.</Text>
                <Text style={styles.subtitle} >Congratulation! We just sent an email to <Text style={{fontWeight: 'bold', color: colors.black}} >{userEmail}</Text>. Please check your inbox to learn more. As a Netflix member, you can watch unlimited TV shows and movies on the mobile app and all your other devices.</Text>
            
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.white
    },
    title: {
        fontSize: 24,
        color: colors.black,
        textAlign: 'center',
        fontFamily: InstagramSans.Bold,
    },
    subtitle: {
        fontSize: 20,
        color: colors.grey,
        fontFamily: InstagramSans.Medium,
        textAlign: 'center',
        marginVertical: 25,
    },
})

export default AccountCreated