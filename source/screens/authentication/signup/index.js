import React, {useState, useEffect} from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, SafeAreaView } from 'react-native'

import LoginButton from '../../../components/button';

import CloseIcon from 'react-native-vector-icons/AntDesign';

import { colors } from '../../../global/colors';
import Input from '../../../components/input';
import { InstagramSans } from '../../../global/fontFamily';



const SignUpScreen = ({navigation}) => {

    const [isLoading, setIsLoading] = useState(false);

    const getStartedFunction = () => {
        setIsLoading(true)

        setTimeout(() => {
            setIsLoading(false)
            navigation.navigate('CreateAccount')
        }, 1000);
    }

    return (
        <SafeAreaView style={styles.container} >
            <View style={{alignItems: 'flex-end', width: '100%', paddingHorizontal: 20,}} >
                <TouchableOpacity
                        onPress={() => {navigation.goBack()}}
                    >
                    <CloseIcon name="close" size={28}  />
                </TouchableOpacity>
            </View>
            <View style={{width: '100%', padding: 20, paddingHorizontal: 30}} >
                <Text style={styles.title} >Ready to watch?</Text>
                <Text style={styles.subtitle} >Enter your email to create or sign in to your account.</Text>
                
                <Input
                    title="Email"
                    placeholder="Enter your email"
                    inputColor={colors.white}
                />
                <LoginButton
                    title="GET STARTED" 
                    loading={isLoading}
                    onPress={() => {getStartedFunction()}}
                />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    },
    title: {
        fontSize: 28,
        color: colors.black,
        textAlign: 'center',
        fontFamily: InstagramSans.Bold,
    },
    subtitle: {
        fontSize: 20,
        color: colors.grey,
        fontFamily: InstagramSans.Medium,
        textAlign: 'center',
        marginVertical: 25,
    },
})

export default SignUpScreen