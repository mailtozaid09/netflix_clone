import React,{useState, useEffect} from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, SafeAreaView, Alert } from 'react-native'
import { colors } from '../../../global/colors'
import { InstagramSans } from '../../../global/fontFamily'
import Input from '../../../components/input'
import LoginButton from '../../../components/button'
import { useDispatch } from 'react-redux'
import OutlinedButton from '../../../components/button/OutlinedButton'
import { signIn } from '../../../store/modules/auth/actions'

const LoginScreen = ({navigation}) => {

    const dispatch = useDispatch();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
  
    const [showPass, setShowPass] = useState(false);
  
    const handleLogin = () => {
      if (!email || !password) {
        Alert.alert('Warning', 'Please, fill in email and password fields');
        return;
      }
  
      dispatch(signIn(email, password));
    }

    

    return (
        <SafeAreaView style={styles.container} >
            <View style={{width: '100%', padding: 20, paddingHorizontal: 30}} >

                <Input
                    title="Email"
                    placeholder="Enter your email"
                    onChangeText={text => setEmail(text)}
                />

                <Input
                    title="Password"
                    placeholder="Enter your password"
                    isPassword={true}
                    showPass={showPass}
                    onChangeText={text => setPassword(text)}
                    onShowPass={() => setShowPass(!showPass)}
                />

                {
                email && password
                ?
                <LoginButton
                    title="Sign In" 
                    onPress={() => {
                        handleLogin()
                        //navigation.navigate('CreateAccount')
                    }}
                />
                :
                <OutlinedButton
                    title="Sign In" 
                    disabled={true}
                    
                />}

                <View>
                    <TouchableOpacity
                        onPress={() => {}}
                    >
                        <Text style={styles.needHelp} >Need help?</Text>
                    </TouchableOpacity>
                    
                    <TouchableOpacity
                        onPress={() => {navigation.navigate('SignUp')}}
                    >
                        <Text style={styles.newToNetflix} >New to Netflix? Sign up Now.</Text>
                    </TouchableOpacity>
                    
                    <Text style={styles.description} >Sign in is protected by Google reCAPTCHA to ensure you're not a bot. Learn more.</Text>
                </View>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.black
    },
    needHelp: {
        fontSize: 18,
        color: colors.white,
        textAlign: 'center',
        marginTop: 20,
        fontFamily: InstagramSans.Light,
    },
    newToNetflix: {
        fontSize: 18,
        color: colors.white,
        textAlign: 'center',
        marginVertical: 30,
        fontFamily: InstagramSans.Regular,
    },
    description: {
        fontSize: 16,
        color: colors.grey,
        textAlign: 'center',
        fontFamily: InstagramSans.Light,
    },
})

export default LoginScreen