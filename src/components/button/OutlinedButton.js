import React from 'react'
import { Text, View, TouchableOpacity, Image, StyleSheet } from 'react-native'
import { colors } from '../../global/colors'
import { media } from '../../global/media'
import { screenWidth } from '../../global/constants'
import { InstagramSans } from '../../global/fontFamily'


const OutlinedButton = ({title, onPress, icon, disabled, buttonColor}) => {

    return (
        <View style={{}} >
            <TouchableOpacity
            onPress={onPress}
            disabled={disabled}
            style={[styles.container, ]}
        >
            {icon && <Image source={icon} style={{height: 24, width: 24, marginRight: 10}} />}
            <Text style={{fontSize: 18, fontFamily: InstagramSans.Medium, color: 'white'}} >{title}</Text>
        </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 50,
        width: '100%',
        borderWidth: 1,
        borderColor: colors.white,
        marginVertical: 10,
        borderRadius: 4,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        
    }
})

export default OutlinedButton