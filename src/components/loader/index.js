import React from 'react'
import { colors } from '../../global/colors'
import { View, ActivityIndicator, StyleSheet } from 'react-native'
import { screenHeight, screenWidth } from '../../global/constants'

const Loader = () => {
    return (
        <View style={styles.container} >
            <ActivityIndicator
                size="large" 
                color={colors.primary}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.black
    }
})

export default Loader