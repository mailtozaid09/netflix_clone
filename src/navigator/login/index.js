import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, ViewBase, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import LoginScreen from '../../screens/authentication/login';
import SignUpScreen from '../../screens/authentication/signup';

import { colors } from '../../global/colors';

const Stack = createStackNavigator();


const LoginStack = ({navgation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Login" 
        >
            <Stack.Screen
                name="Login"
                component={LoginScreen}
                options={({navigation}) => ({
                    // headerShown: true,
                    // headerTitle: '',
                    // headerStyle: styles.headerStyle,
                    // headerLeft: () => <Header navigation={navigation} />,
                })}
            />
            <Stack.Screen
                name="SignUp"
                component={SignUpScreen}
                options={({navigation}) => ({
                    // headerShown: false,
                    // headerStyle: styles.headerStyle,
                    // headerTitle: '',
                    // headerLeft: () => null,
                    // headerTitle: (props) => 
                    // <Image source={media.netflixText} style={{height: 30, width: 140}} />
                })}
            />
           
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    headerStyle: {
        backgroundColor: colors.black,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0,
    }
})

export default LoginStack