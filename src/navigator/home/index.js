import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from '../../screens/dashboard/home';
import { colors } from '../../global/colors';


const Stack = createStackNavigator();


const HomeStack = ({navigation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Home" 
        >
            
            <Stack.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    headerShown: false,
                    // headerTitle: '',
                    // headerRight: () => null,
                    // headerLeft: () =>  (<HomeHeader navigation={navigation} />),
                }}
            />
            {/* <Stack.Screen
                name="MoviesDetails"
                component={MoviesDetailsScreen}
                options={{
                    headerShown: false,
                    // headerTitle: '',
                    // headerRight: () => null,
                    // headerLeft: () =>  (<HomeHeader navigation={navigation} />),
                }}
            />
            <Stack.Screen
                name="ViewVideo"
                component={ViewVideoScreen}
                options={{
                    headerShown: false,
                    // headerTitle: '',
                    // headerRight: () => null,
                    // headerLeft: () =>  (<HomeHeader navigation={navigation} />),
                }}
            /> */}
            {/* <Stack.Screen
                name="Search"
                component={SearchScreen}
                options={({navigation}) => ({
                    headerShown: true,
                    headerTitle: '',
                    headerStyle: [styles.headerStyle, {backgroundColor: colors.black}],
                    headerLeft: () => null,
                    headerTitle: () => <SearchHeader navigation={navigation} />,
                })}
            />
            <Stack.Screen
                name="ProfileSettings"
                component={ProfileSettings}
                options={({navigation}) => ({
                    headerShown: true,
                    headerTitle: '',
                    headerStyle: [styles.headerStyle, {backgroundColor: colors.black}],
                    headerLeft: () => null,
                    headerTitle: () => <ProfileHeader navigation={navigation} />,
                })}
            /> */}
            
            
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    headerStyle: {
        backgroundColor: colors.black,
        elevation: 0,
        shadowOpacity: 0,
        borderBottomWidth: 0,
    }
})


export default HomeStack