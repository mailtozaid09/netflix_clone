import React, { useState, useEffect } from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
 
import Swiper from 'react-native-swiper'

import { onboarding_data } from '../../global/sampleData'
import { colors } from '../../global/colors'
import LoginButton from '../../components/button'
import { InstagramSans } from '../../global/fontFamily'
import { useDispatch, useSelector } from 'react-redux'
import AsyncStorage from '@react-native-async-storage/async-storage'
import Loader from '../../components/loader'
import { screenLoader } from '../../store/modules/home/actions'


const OnBoardingScreen = ({navigation}) => {

    const dispatch = useDispatch()

    const [userDetails, setUserDetails] = useState({});

    const user = useSelector(state => state.auth.user);

    const loader = useSelector(state => state.home.loader);

    const current_profile = useSelector(state => state.profile.current_profile);
    const profile_list = useSelector(state => state.profile.profile_list);

    //console.log("current_profile ", current_profile);

    //console.log("user ", user);

    console.log("profile_list ", profile_list);

    
    useEffect(() => {
        dispatch(screenLoader(true))

        getUserDetails()
    }, [user])
    

    const getUserDetails = async () => {
        let user = await AsyncStorage.getItem('user_details');  
        let user_details = JSON.parse(user);  
        setUserDetails(user_details)

        if(user != null){
            navigation.navigate('WhoIsWatching')
            setTimeout(() => {
                dispatch(screenLoader(false))
            }, 1000);
        }else{
            setTimeout(() => {
                dispatch(screenLoader(false))
            }, 1000);
            
            navigation.navigate('OnboardingStack', {screen:'OnBoarding'})
        }
    }
   


    return (
        <>
            {loader
                ?
                <Loader />
                :
                <>
                <Swiper 
                loop={false}
                style={styles.wrapper} 
                showsButtons={false}
                dotColor={colors.grey}
                activeDotColor={colors.primary}
                paginationStyle={{ position: 'absolute', bottom: 120}}
            >
            {onboarding_data.map((item, index) => (
                <View 
                    key={index}
                    style={styles.itemContainer}>
                    <Image source={item.image} style={{height: index == 3 ? '100%' : 220, width: index == 3 ? '100%' : 220}} />
                    <View style={[index == 3 ? {position: 'absolute'} : null, {marginHorizontal: 20, alignItems: 'center', justifyContent: 'center'}]} >
                        <Text style={styles.text}>{item.title}</Text>
                        <Text style={styles.description}>{item.description}</Text>
                    </View>
                    
                </View>
            ))}
            </Swiper>
            <View style={{width: '100%', position: 'absolute', bottom: 25, padding: 20}} >
               <LoginButton
                    title="GET STARTED" 
                    onPress={() => navigation.navigate('LoginStack', {screen: 'SignUp'})}
                />
            </View>
            </>
            }
        </>
    )
}


const styles = StyleSheet.create({
    wrapper: {},
    itemContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#000'
    },
    text: {
        color: '#fff',
        fontSize: 30,
        textAlign: 'center',
        marginBottom: 20,
        fontFamily: InstagramSans.Bold,
    },
    description: {
        color: '#fff',
        fontSize: 20,
        textAlign: 'center',
        fontFamily: InstagramSans.Regular,
    }
  })
   
export default OnBoardingScreen