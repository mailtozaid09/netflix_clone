// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getDatabase } from "firebase/database";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyB1PenH6iufqu_WdGdsiyNcAU22esatm-Y",
  authDomain: "eshop-89796.firebaseapp.com",
  databaseURL: "https://eshop-89796-default-rtdb.firebaseio.com",
  projectId: "eshop-89796",
  storageBucket: "eshop-89796.appspot.com",
  messagingSenderId: "737888285144",
  appId: "1:737888285144:web:6069d1ae766e47e12502ad",
  measurementId: "G-BR3FRM5B82"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

export const db = getDatabase(app)