export const InstagramSans = {
    Bold: 'InstagramSans-Bold',
    Light: 'InstagramSans-Light',
    Medium: 'InstagramSans-Medium',
    Regular: 'InstagramSans-Regular',
};
  

export const fontSize = {
  Heading: 30,
  SubHeading: 24,
  Title: 20,
  SubTitle: 16,
  Body: 14,
}