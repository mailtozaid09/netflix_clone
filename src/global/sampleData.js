import { colors } from "./colors";
import { media } from "./media";

export const users = [
  {
    id: 3,
    name: 'Zaid',
    email: 'zaid',
    phone: '55997386673',
    address: 'Rionegro, Colômbia',
    password: 'zaid',
  },
  {
    id: 1,
    name: 'Pablo Escobar',
    email: 'pablo@escobar.com',
    phone: '55997386673',
    address: 'Rionegro, Colômbia',
    password: 'pablo123',
  },
  {
    id: 2,
    name: 'Michael Scofield',
    email: 'michael@scofield.com',
    phone: '55992526914',
    address: 'Oxfordshire, Inglaterra',
    password: 'michael123',
  },
];


export const movies = [
    {
      id: 1,
      title: 'Stranger Things',
      image: require('../assets/images/movies/strangerthings.jpg'),
    },
    {
      id: 2,
      title: 'Deadpool 2',
      image: require('../assets/images/movies/deadpool.jpg'),
    },
    {
      id: 3,
      title: 'Mean Girls',
      image: require('../assets/images/movies/meangirls.jpg'),
    },
    {
      id: 4,
      title: 'Moonlight',
      image: require('../assets/images/movies/moonlight.jpg'),
    },
    {
      id: 5,
      title: 'Mr Bean',
      image: require('../assets/images/movies/mrbean.jpg'),
    },
    {
      id: 6,
      title: 'Whitehouse',
      image: require('../assets/images/movies/whitehouse.jpg'),
    },
    {
      id: 7,
      title: 'Jaws',
      image: require('../assets/images/movies/jaws.jpg'),
    },
  ];

export const onboarding_data = [
    {
        id: 1,
        image: media.onboarding1,
        title: 'Watch on any device',
        description: 'Stream on your phone, tablet, laptop, and TV without payning more.'
    },
    {
        id: 2,
        image: media.onboarding2,
        title: 'Download and go',
        description: 'Save your data, watch offline on a plane, train, or submarine...'
    },
    {
        id: 3,
        image: media.onboarding3,
        title: 'No pesky contracts',
        description: 'Join today, cancel anytime.'
    },
    {
      id: 4,
      image: media.onboarding4,
      title: 'How do I watch?',
      description: 'Members that subscribe to Netflix can wathc here in the app.'
  },
]


export const newsAndHotTabs = [
  {
      id: 1,
      image: media.comingsoon,
      title: 'Coming Soon',
     
  },
  {
      id: 2,
      image: media.everyonewatching,
      title: "Everyone's Watching",
   
  },
  {
      id: 3,
      image: media.top10,
      title: 'Top 10',
  },
]